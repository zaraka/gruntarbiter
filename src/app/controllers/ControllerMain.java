package app.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ControllerMain {

   private static final Logger LOG = LoggerFactory.getLogger(ControllerMain.class);

    //------------------------object injections
    @FXML
    private TableView tableView_masterTable;

    //------------------------method hooks
    @FXML
    private void addCharacter() {
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("views/addCharacter.fxml"));
            Stage dialog = new Stage();
            dialog.setTitle("Create new character");
            dialog.setScene(new Scene(root));
            dialog.showAndWait();
        } catch (IOException ex) {
            LOG.error("Could not load addCharacter dialog: ", ex);
        }


    }
}
