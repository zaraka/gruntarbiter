package app.models;

public class Host {
    private int ratting;

    private int attack;

    private int sleeze;

    private int firewall;

    private int dataProcessing;

    private int overwatchScore;

    public int getRatting() {
        return ratting;
    }

    public void setRatting(int ratting) {
        this.ratting = ratting;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getSleeze() {
        return sleeze;
    }

    public void setSleeze(int sleeze) {
        this.sleeze = sleeze;
    }

    public int getFirewall() {
        return firewall;
    }

    public void setFirewall(int firewall) {
        this.firewall = firewall;
    }

    public int getDataProcessing() {
        return dataProcessing;
    }

    public void setDataProcessing(int dataProcessing) {
        this.dataProcessing = dataProcessing;
    }

    public int getOverwatchScore() {
        return overwatchScore;
    }

    public void setOverwatchScore(int overwatchScore) {
        this.overwatchScore = overwatchScore;
    }
}
